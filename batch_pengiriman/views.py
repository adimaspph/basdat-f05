from django.shortcuts import render
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .forms import *

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def toList(list_tuple):
    result = []
    for tuple in list_tuple:
        kamus = dict()
        kamus["no_transaksi_sumber_daya"] = tuple.no_transaksi_sumber_daya
        kamus['nama'] = tuple.nama
        kamus['id_lokasi'] = tuple.id_lokasi
        kamus['total_berat'] = tuple.total_berat
        kamus['status_permohonan'] = tuple.status_permohonan[-3:]
        if tuple.status_pengiriman:
            kamus['status_pengiriman'] = tuple.status_pengiriman[-3:]
        result.append(kamus)
    return result


def read_batch(request):
    with connection.cursor() as c:
        c.execute("Set search_path to sidia;")
        c.execute("""select Distinct no_transaksi_sumber_daya, nama, id_lokasi, total_berat, string_agg(kode_status_permohonan, ', '
                    ORDER BY rsp.tanggal) status_permohonan, string_agg(kode_status_batch_pengiriman, ', ' ORDER BY sp.tanggal) status_pengiriman
                    FROM permohonan_sumber_daya_faskes psdf join pengguna p
                    ON psdf.username_petugas_faskes=p.username
                    JOIN faskes f ON f.username_petugas=psdf.username_petugas_faskes
                    JOIN transaksi_sumber_daya tsd ON tsd.nomor=psdf.no_transaksi_sumber_daya
                    LEFT JOIN riwayat_status_permohonan rsp ON rsp.nomor_permohonan=psdf.no_transaksi_sumber_daya
                    JOIN batch_pengiriman bp ON tsd.nomor=bp.nomor_transaksi_sumber_daya
                    LEFT JOIN riwayat_status_pengiriman sp ON sp.kode_batch=bp.kode
                    GROUP BY no_transaksi_sumber_daya, nama, id_lokasi, total_berat
                    ORDER BY no_transaksi_sumber_daya ASC;""")
        hasil = namedtuplefetchall(c)
        # print(hasil)
        c.execute("Set search_path to public;")
        data_batch = toList(hasil)

    response = {"data_batch": data_batch}
    return render(request, "read_batch.html", response)

def generate_kode_bp():
    with connection.cursor() as c:
        c.execute("Set search_path to sidia;")
        c.execute("SELECT kode FROM batch_pengiriman ORDER BY kode DESC LIMIT 1;")
        last_id = namedtuplefetchall(c)
        c.execute("Set search_path to public;")
        if len(last_id) == 0:
            return "bp001"
        number = last_id[0].kode.split("bp")

    return "bp{0:03d}".format((int(number[1]) + 1))

def generat_alamat(id):
    with connection.cursor() as c:
        c.execute("Set search_path to sidia;")
        c.execute("""SELECT provinsi, kabkot, kecamatan, kelurahan, jalan_no
        FROM lokasi
        WHERE id='{}';""".format(id))
        hasil = namedtuplefetchall(c)
        c.execute("Set search_path to public;")

        if hasil:
            return "{}, {}, {}, {}, {}".format(hasil[0].provinsi, hasil[0].kabkot, hasil[0].kecamatan, hasil[0].kelurahan, hasil[0].jalan_no)

    return "Lokasi tidak ada"

def get_username(nama):
    with connection.cursor() as c:
        c.execute("Set search_path to sidia;")
        c.execute("SELECT username from pengguna where nama='{}';".format(nama))
        hasil = namedtuplefetchall(c)
        c.execute("Set search_path to public;")

        if len(hasil) == 1:
            return hasil[0].username

    return "username tidak ditemukan"

def create_batch(request, my_id, nama):
    if request.method == 'POST':
        form = createBatchForm(request.POST)
        if form.is_valid():
            kode = generate_kode_bp()
            no = my_id
            satgas = get_username(nama)

            supir = form['petugas'].value()
            kendaraan = form['kendaraan'].value()

            asal = form['asal'].value()
            tujuan = form['tujuan'].value()
            tanda_terima = "Diterima"

            with connection.cursor() as c:
                c.execute("Set search_path to sidia;")
                c.execute("""INSERT INTO batch_pengiriman values
                ('{}','{}','{}','{}','{}','{}','{}','{}');""".format(kode, satgas, supir, tanda_terima, no, asal, tujuan, kendaraan))
                c.execute("Set search_path to public;")

    with connection.cursor() as c:
        c.execute("Set search_path to sidia;")
        c.execute("""SELECT kode, k.nama as kendaraan, p.nama as petugas, id_lokasi_asal, id_lokasi_tujuan
        FROM batch_pengiriman bp JOIN pengguna p ON username=username_petugas_distribusi
        JOIN kendaraan k ON bp.no_kendaraan=k.nomor
        WHERE nomor_transaksi_sumber_daya='{}';""".format(my_id))
        hasil = namedtuplefetchall(c)
        c.execute("Set search_path to public;")

    form = createBatchForm()
    response = {"form" : form, "kode_bp": generate_kode_bp(), "no":my_id, "nama":nama, "data_batch_tentu":hasil}
    return render(request, "create_batch.html", response)

def tambah_batch(request):

    return HttpResponseRedirect(reverse('batch_pengiriman:read_batch'))