from django import forms

kendaraan = ['Truck Telolet', 'Minibus', 'Kereta Thomas', 'Mobil', 'Kapal']

def get_jenis_kendaraan():
    result = [tuple([i, i]) for i in kendaraan]
    return result

class addKendaraanForm(forms.Form):

    nomor_kendaraan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nomor Kendaraan',
        'type' : 'text',
        'required': True,
    }))

    nama_kendaraan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Kendaraan',
        'type': 'text',
        'required': True,
    }))

    jenis_kendaraan = forms.CharField(label="jenis_kendaraan", widget=forms.Select(choices=get_jenis_kendaraan()))

    berat_maksimum = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Berat Maksimum Kendaraan (Kg)',
        'type': 'number',
        'required': True,
    }))

