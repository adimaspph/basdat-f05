from django import forms

prov = ['Aceh', 'Sumatera Utara', 'Sumatera Barat', 'Riau', 'Jambi', 'Sumatera Selatan', 'Bengkulu', 'Lampung', 'Kepulauan Bangka Belitung', 'Kepulauan Riau', 'Dki Jakarta', 'Jawa Barat', 'Jawa Tengah', 'Di Yogyakarta', 'Jawa Timur', 'Banten', 'Bali', 'Nusa Tenggara Barat', 'Nusa Tenggara Timur', 'Kalimantan Barat', 'Kalimantan Tengah', 'Kalimantan Selatan', 'Kalimantan Timur', 'Kalimantan Utara', 'Sulawesi Utara', 'Sulawesi Tengah', 'Sulawesi Selatan', 'Sulawesi Tenggara', 'Gorontalo', 'Sulawesi Barat', 'Maluku', 'Maluku Utara', 'Papua Barat', 'Papua']

def get_prov():
    result = [tuple([i, i]) for i in prov]
    return result


class registerAdminForm(forms.Form):

    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Username',
        'type' : 'text',
        'required': True,
    }))

    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'type' : 'password',
        'placeholder': "Password",
        'required': True,
    }))

    nama_lengkap = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Lengkap',
        'type' : 'text',
        'required': True,
    }))

    kelurahan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kelurahan',
        'type' : 'text',
        'required': True,
    }))

    kecamatan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kecamatan',
        'type' : 'text',
        'required': True,
    }))

    kabupaten = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kabupaten/Kota',
        'type' : 'text',
        'required': True,
    }))

    provinsi = forms.CharField(label="Provinsi", widget=forms.Select(choices=get_prov()))

    no_telepon = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nomor Telepon ',
        'type' : 'text',
        'required': False,
    }))

class registerPFForm(forms.Form):

    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Username',
        'type' : 'text',
        'required': True,
    }))

    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'type' : 'password',
        'placeholder': "Password",
        'required': True,
    }))

    nama_lengkap = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Lengkap',
        'type' : 'text',
        'required': True,
    }))

    kelurahan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kelurahan',
        'type' : 'text',
        'required': True,
    }))

    kecamatan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kecamatan',
        'type' : 'text',
        'required': True,
    }))

    kabupaten = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kabupaten/Kota',
        'type' : 'text',
        'required': True,
    }))

    provinsi = forms.CharField(label="Provinsi", widget=forms.Select(choices=get_prov()))

    no_telepon = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nomor Telepon ',
        'type' : 'text',
        'required': False,
    }))

class registerPDForm(forms.Form):

    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Username',
        'type' : 'text',
        'required': True,
    }))

    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'type' : 'password',
        'placeholder': "Password",
        'required': True,
    }))

    nama_lengkap = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Lengkap',
        'type' : 'text',
        'required': True,
    }))

    kelurahan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kelurahan',
        'type' : 'text',
        'required': True,
    }))

    kecamatan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kecamatan',
        'type' : 'text',
        'required': True,
    }))

    kabupaten = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kabupaten/Kota',
        'type' : 'text',
        'required': True,
    }))

    provinsi = forms.CharField(label="Provinsi", widget=forms.Select(choices=get_prov()))

    no_telepon = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nomor Telepon ',
        'type' : 'text',
        'required': False,
    }))

    no_sim = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nomor SIM ',
        'type' : 'text',
        'required': False,
    }))

class registerSupForm(forms.Form):
    organisasi = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Organisasi',
        'type': 'text',
        'required': True,
    }))
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Username',
        'type' : 'text',
        'required': True,
    }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'type' : 'password',
        'placeholder': "Password",
        'required': True,
    }))
    nama_lengkap = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Lengkap',
        'type' : 'text',
        'required': True,
    }))
    kelurahan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kelurahan',
        'type' : 'text',
        'required': True,
    }))
    kecamatan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kecamatan',
        'type' : 'text',
        'required': True,
    }))
    kabupaten = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kabupaten/Kota',
        'type' : 'text',
        'required': True,
    }))
    provinsi = forms.CharField(label="Provinsi", widget=forms.Select(choices=get_prov()))

    no_telepon = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nomor Telepon',
        'type' : 'text',
        'required': False,
    }))

class registerLoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Username',
        'type': 'text',
        'required': True,
    }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'type': 'password',
        'placeholder': "Password",
        'required': True,
    }))