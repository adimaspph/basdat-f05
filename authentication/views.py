from django.shortcuts import render
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .forms import *

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def check_user_is_exist(jenis, param):
    c = connection.cursor()
    c.execute("Set search_path to sidia;")

    if jenis == "username":
        c.execute(f"select username from pengguna where username = '{param}';")

    hasil = namedtuplefetchall(c)

    connection.close()
    if len(hasil) == 1:
        return True

    return False

def register_pengguna(username, password, nama, alamat_kel, alamat_kec, alamat_kabkot, alamat_prov, no_telepon, role, tambahan):
    c = connection.cursor()
    c.execute("Set search_path to sidia;")
    c.execute("INSERT INTO PENGGUNA VALUES ('{}','{}','{}','{}','{}','{}','{}','{}');".format(username, password, nama, alamat_kel, alamat_kec, alamat_kabkot, alamat_prov, no_telepon))

    if role == "supplier":
        c.execute("INSERT INTO SUPPLIER VALUES ('{}','{}');".format(username, tambahan))

    elif role == "admin satgas":
        c.execute("INSERT INTO admin_satgas VALUES ('{}');".format(username))

    elif role == "petugas distribusi":
        c.execute("INSERT INTO petugas_distribusi VALUES ('{}','{}');".format(username, tambahan))

    elif role == "petugas faskes":
        c.execute("INSERT INTO petugas_faskes VALUES ('{}');".format(username))

    connection.close()


def index(request):
    response = {}
    try:
        username = request.session['username']
        return HttpResponseRedirect(reverse('authentication:dashboard'))

    except:
        return render(request, 'index.html', response)

def register(request):
    response = {}
    return render(request,'register.html',response)

def register_admin(request):
    if request.method == 'POST':
        registform = registerAdminForm(request.POST)

        if registform.is_valid():
            username = registform.cleaned_data['username']
            password = registform.cleaned_data['password']
            nama = registform.cleaned_data['nama_lengkap']
            kelurahan = registform.cleaned_data['kelurahan']
            kecamatan = registform.cleaned_data['kecamatan']
            kabupaten = registform.cleaned_data['kabupaten']
            provinsi = registform.cleaned_data['provinsi']
            no_telepon = registform.cleaned_data['no_telepon']

            if check_user_is_exist("username", username) == False:
                register_pengguna(username, password, nama, kelurahan, kecamatan, kabupaten, provinsi, no_telepon, "admin satgas", "-")
                request.session.flush()
                request.session['username'] = username
                request.session['password'] = password
                request.session['nama'] = nama
                request.session['kelurahan'] = kelurahan
                request.session['kecamatan'] = kecamatan
                request.session['kabupaten'] = kabupaten
                request.session['provinsi'] = provinsi
                request.session['no_telepon'] = no_telepon
                request.session['role'] = 'admin satgas'
                return HttpResponseRedirect(reverse('authentication:index'))

            else:
                messages.error(request, 'Username sudah terpakai')
                return HttpResponseRedirect(reverse('authentication:register_admin'))

    register_form = registerAdminForm()
    response = {'register_form':register_form}
    return render(request,'register_admin.html',response)

def register_petugas_faskes(request):
    if request.method == 'POST':
        registform = registerPFForm(request.POST)

        if registform.is_valid():
            username = registform.cleaned_data['username']
            password = registform.cleaned_data['password']
            nama = registform.cleaned_data['nama_lengkap']
            kelurahan = registform.cleaned_data['kelurahan']
            kecamatan = registform.cleaned_data['kecamatan']
            kabupaten = registform.cleaned_data['kabupaten']
            provinsi = registform.cleaned_data['provinsi']
            no_telepon = registform.cleaned_data['no_telepon']

            if check_user_is_exist("username", username) == False:
                register_pengguna(username, password, nama, kelurahan, kecamatan, kabupaten, provinsi, no_telepon, "petugas faskes", "-")
                request.session.flush()
                request.session['username'] = username
                request.session['password'] = password
                request.session['nama'] = nama
                request.session['kelurahan'] = kelurahan
                request.session['kecamatan'] = kecamatan
                request.session['kabupaten'] = kabupaten
                request.session['provinsi'] = provinsi
                request.session['no_telepon'] = no_telepon
                request.session['role'] = 'petugas faskes'
                return HttpResponseRedirect(reverse('authentication:index'))

            else:
                messages.error(request, 'Username sudah terpakai')
                return HttpResponseRedirect(reverse('authentication:register_petugas_faskes'))

    register_form = registerPFForm()
    response = {'register_form':register_form}
    return render(request,'register_petugas_faskes.html',response)

def register_supplier(request):
    if request.method == 'POST':
        registform = registerSupForm(request.POST)
        if registform.is_valid():
            username = registform.cleaned_data['username']
            organisasi = registform.cleaned_data['organisasi']
            password = registform.cleaned_data['password']
            nama = registform.cleaned_data['nama_lengkap']
            kelurahan = registform.cleaned_data['kelurahan']
            kecamatan = registform.cleaned_data['kecamatan']
            kabupaten = registform.cleaned_data['kabupaten']
            provinsi = registform.cleaned_data['provinsi']
            no_telepon = registform.cleaned_data['no_telepon']

            if check_user_is_exist("username", username) == False:
                register_pengguna(username, password, nama, kelurahan, kecamatan, kabupaten, provinsi, no_telepon, "supplier", organisasi)
                request.session.flush()
                request.session['username'] = username
                request.session['password'] = password
                request.session['nama'] = nama
                request.session['kelurahan'] = kelurahan
                request.session['kecamatan'] = kecamatan
                request.session['kabupaten'] = kabupaten
                request.session['provinsi'] = provinsi
                request.session['no_telepon'] = no_telepon
                request.session['role'] = 'supplier'
                request.session['organisasi'] = organisasi

                return HttpResponseRedirect(reverse('authentication:dashboard'))

            else:
                messages.error(request, 'Username sudah terpakai')

    register_form = registerSupForm()
    response = {'register_form': register_form}
    return render(request,'register_supplier3.html',response)

def register_petugas_distribusi(request):
    if request.method == 'POST':
        registform = registerPDForm(request.POST)
        if registform.is_valid():
            email = registform.cleaned_data['email']
            password = registform.cleaned_data['password']
            nama = registform.cleaned_data['nama_lengkap']
            no_telp = registform.cleaned_data['no_telepon']
            if check_user_is_exist("PENGGUNA",email) == False:
                register(email,password,nama,no_telp)
                request.session.flush()
                with connection.cursor() as c:
                    c.execute("INSERT INTO DISTRIBUSI VALUES (%s)",[email])
                    c.execute("INSERT INTO PETUGAS_DISTRIBUSI VALUES (%s, %s)",[email,None])
                request.session['email'] = email
                request.session['nama_lengkap'] = nama
                request.session['telepon'] = no_telp
                request.session['password'] = password
                request.session['role'] = 'petugas distribusi'
                return HttpResponseRedirect(reverse('homepage:homepage'))
            else:
                messages.error(request, 'Email sudah terdaftar')
                return HttpResponseRedirect(reverse('auth:register_admin'))
    register_form = registerPDForm()
    response = {'register_form':register_form}

    return render(request,'register_admin.html',response)


def login(request):
    if request.method == 'POST':
        registform = registerLoginForm(request.POST)
        if registform.is_valid():
            username = registform.cleaned_data['username']
            password = registform.cleaned_data['password']

            c = connection.cursor()
            c.execute("Set search_path to sidia;")
            c.execute("select * from pengguna where username='{}' and password='{}';".format(username, password))
            hasil = namedtuplefetchall(c)

            c.execute("set search_path to public")

            if len(hasil) == 1:
                request.session.flush()
                role = cek_role(username)
                request.session['username'] = username
                request.session['password'] = password
                request.session['nama'] = hasil[0].nama
                request.session['kelurahan'] = hasil[0].alamat_kel
                request.session['kecamatan'] = hasil[0].alamat_kec
                request.session['kabupaten'] = hasil[0].alamat_kabkot
                request.session['provinsi'] = hasil[0].alamat_prov
                request.session['no_telepon'] = hasil[0].no_telepon
                request.session['role'] = role

                if role == "supplier":
                    c.execute("Set search_path to sidia;")
                    c.execute("select * from supplier where username='{}';".format(username))
                    hasil = namedtuplefetchall(c)
                    c.execute("set search_path to public")
                    print('masuk')
                    request.session['organisasi'] = hasil[0].nama_organisasi

                elif role == "petugas distribusi":
                    c.execute("Set search_path to sidia;")
                    c.execute("select * from petugas_distribusi where username='{}';".format(username))
                    hasil = namedtuplefetchall(c)
                    c.execute("set search_path to public")
                    request.session['sim'] = hasil[0].no_sim

                elif role == "admin satgas":
                    c.execute("Set search_path to sidia;")
                    c.execute("select * from admin_satgas where username='{}';".format(username))
                    hasil = namedtuplefetchall(c)
                    c.execute("set search_path to public")

                elif role == "petugas faskes":
                    c.execute("Set search_path to sidia;")
                    c.execute("select * from petugas_faskes where username='{}';".format(username))
                    hasil = namedtuplefetchall(c)
                    c.execute("set search_path to public")

                print(hasil)
                c.close()
                return HttpResponseRedirect(reverse('authentication:dashboard'))

            else:
                messages.error(request, 'Username atau password salah')

    form = registerLoginForm()
    response = {'form': form}
    return render(request, 'login.html', response)

def cek_role(username):
    result = "gk tau"
    c = connection.cursor()
    c.execute("Set search_path to sidia;")

    c.execute("select username from admin_satgas where username='{}';".format(username))
    hasil = namedtuplefetchall(c)
    if hasil != []:
        c.close()
        return "admin satgas"

    c.execute("select username from supplier where username='{}';".format(username))
    hasil = namedtuplefetchall(c)
    if hasil != []:
        c.close()
        return "supplier"

    c.execute("select username from petugas_distribusi where username='{}';".format(username))
    hasil = namedtuplefetchall(c)
    if hasil != []:
        c.close()
        return "petugas distribusi"

    c.execute("select username from petugas_faskes where username='{}';".format(username))
    hasil = namedtuplefetchall(c)
    if hasil != []:
        c.close()
        return "petugas faskes"

    return result

def logout(request):
    request.session.flush()
    return render(request,'index.html')

def dashboard(request):

    response = {}
    return render(request,'dashboard.html',response)


