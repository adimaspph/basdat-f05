from django.urls import path
from .views import *

app_name = 'stokfaskes'

urlpatterns = [
    path('stokfaskes/create', create_stokfaskes, name='create_stokfaskes'),
	path('stokfaskes/read', read_stokfaskes, name='read_stokfaskes'),
	path('stokfaskes/update/<str:my_id>', update_stokfaskes, name='update'),
	path('stokfaskes/delete/<str:my_id>', delete_stokfaskes, name='delete'),
]