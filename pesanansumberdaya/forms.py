from django import forms
from django.db import connection
from collections import namedtuple

initial = []

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def get_supplier():
    with connection.cursor() as c:
        c.execute("Set search_path to sidia;")
        c.execute("SELECT * from supplier WHERE username IN (SELECT username_supplier FROM item_sumber_daya);")
        hasil = namedtuplefetchall(c)
        c.execute("Set search_path to public;")
    
    result = [tuple([hasil[i].username , hasil[i].nama_organisasi]) for i in range(len(hasil))]
    return result

def get_initial():
    result = [tuple([i, i]) for i in initial]
    return result


class addDaftarItemForm(forms.Form):

    no_urut = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Kendaraan',
        'type': 'int',
        'required': True,
        'readonly': True,
    }))

    supplier = forms.CharField(label="supplier", widget=forms.Select(choices=get_supplier()))

    kode_item = forms.CharField(label="kode_item", widget=forms.Select(choices=get_initial()))

    jumlah_item = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'jumlah_item',
        'type': 'int',
        'required': True,
    }))

