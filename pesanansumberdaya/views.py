from django.shortcuts import render
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .forms import *
import sys
import datetime
import django

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def generate_id_pesanan_sumber_daya():
    with connection.cursor() as c:
        c.execute("Set search_path to sidia;")
        c.execute("SELECT nomor FROM transaksi_sumber_daya ORDER BY nomor DESC LIMIT 1;")
        last_id = namedtuplefetchall(c)
        c.execute("Set search_path to public;")
        if len(last_id) == 0:
            return 1
        
        number = int(last_id[0].nomor) + 1

    return number


def get_item_by_supplier(request):
    supplier_username = request.GET.get('supplier')
    with connection.cursor() as c:
        c.execute("Set search_path to sidia;")
        c.execute("SELECT * from item_sumber_daya WHERE username_supplier = '{}';".format(supplier_username))
        hasil = namedtuplefetchall(c)
        c.execute("Set search_path to public;")

    print(supplier_username)
    return render(request, 'ajax_updateitembysupplier.html', {'items': hasil})

def create_pesanansumberdaya(request):
    if request.method == 'POST':
        form = addDaftarItemForm(request.POST)
        if form.is_valid():
            no_urut = form.cleaned_data['no_urut']
            supplier = form.cleaned_data['supplier']
            kode_item = form.cleaned_data['kode_item']
            jumlah_item = form.cleaned_data['jumlah_item']

            response = {}
            
            try:
                with connection.cursor() as c:
                    if int(no_urut) == 1:
                        nomor_pesanan = generate_id_pesanan_sumber_daya()
                        petugas = request.session['username']
                        tanggal = datetime.datetime.now()
                        c.execute("Set search_path to sidia;")
                        c.execute("INSERT INTO TRANSAKSI_SUMBER_DAYA VALUES({}, '{}', NULL, NULL);".format(nomor_pesanan, tanggal))
                        c.execute("INSERT INTO PESANAN_SUMBER_DAYA VALUES ({}, '{}', {});".format(nomor_pesanan, petugas, 0))
                        c.execute("INSERT INTO DAFTAR_ITEM VALUES ({}, {}, {}, '{}', NULL, NULL);".format(nomor_pesanan, no_urut, jumlah_item, kode_item))
                        
                    nomor_pesanan = generate_id_pesanan_sumber_daya()
                    petugas = request.session['username']
                    initial_data = {'no_urut': 1}

                    form = addDaftarItemForm(initial = initial_data)
                    response = {'form': form, 'id': nomor_pesanan, 'petugas': petugas}
            except:
                print("Unexpected error:", sys.exc_info()[0])
                # messages.error(request, 'PENAMBAHAN GAGAL')
                return HttpResponseRedirect(reverse('pesanansumberdaya:read_pesanansumberdaya'))
            
            print("berhasil")
            return render(request, 'create_pesanansumberdaya.html', response)
            # print("INSERT INTO KENDARAAN VALUES ({}, "{}", "{}", {});".format(nomor_kendaraan, nama_kendaraan, jenis_kendaraan, berat_maksimum))

            # try:
            #     with connection.cursor() as c:
            #         c.execute("Set search_path to sidia;")
            #         c.execute("INSERT INTO KENDARAAN VALUES ('{}', '{}', '{}', '{}');".format(nomor_kendaraan, nama_kendaraan, jenis_kendaraan, berat_maksimum))
            #         # hasil = namedtuplefetchall(c)
            # except:
            #     messages.error(request, 'PENAMBAHAN GAGAL')
            #     return HttpResponseRedirect(reverse('kendaraan:add_kendaraan'))

            

    initial_data = {'no_urut': 1}
    nomor_pesanan = generate_id_pesanan_sumber_daya()
    petugas = request.session['username']

    form = addDaftarItemForm(initial = initial_data)
    response = {'form': form, 'id': nomor_pesanan, 'petugas': petugas}
    return render(request, 'create_pesanansumberdaya.html', response)

def read_pesanansumberdaya(request):
    with connection.cursor() as c:
        c.execute("Set search_path to sidia;")
        c.execute("select * from pesanan_sumber_daya left outer join transaksi_sumber_daya on nomor_pesanan = nomor;")
        hasil = namedtuplefetchall(c)
        print(hasil)
        print(type(hasil[0]))
        c.execute("Set search_path to public;")
        data_pesanansumberdaya = hasil

    response = {"data_pesanansumberdaya" : data_pesanansumberdaya}
    return render(request, 'read_pesanansumberdaya.html', response)

def detail_pesanansumberdaya(request, my_id):
    with connection.cursor() as c:
        c.execute("Set search_path to sidia;")
        c.execute("select * from pesanan_sumber_daya left outer join transaksi_sumber_daya on nomor_pesanan = nomor;")
        hasil = namedtuplefetchall(c)
        print(hasil)
        detail_item = -1
        for item in hasil:
            if str(item.nomor) == my_id:
                detail_item = item
                break
        print(my_id)
        print(detail_item)

        c.execute("select * from daftar_item where no_transaksi_sumber_daya = %s;", my_id)
        hasil = namedtuplefetchall(c)
        print(hasil)
        daftar_pesanan = hasil
    
        kode_item = hasil[0].kode_item_sumber_daya
        c.execute("select * from item_sumber_daya where kode = '{}';".format(kode_item))
        hasil = namedtuplefetchall(c)
        print(hasil)
        itemsup = hasil[0]
        
        c.execute("Set search_path to public;")

    response = {
                "id" : my_id,
                "petugas" : detail_item.username_admin_satgas,
                "tanggal" : detail_item.tanggal,
                "username_supplier" : itemsup.username_supplier,
                "total_harga" : detail_item.total_harga,
                "daftar_pesanan_item" : daftar_pesanan}
    return render(request, 'detail_pesanansumberdaya.html', response)


def riwayat_pesanansumberdaya(request):
    with connection.cursor() as c:
        c.execute("Set search_path to sidia;")
        c.execute("select * from item_sumber_daya;")
        hasil = namedtuplefetchall(c)
        print(hasil)
        c.execute("Set search_path to public;")
        data_pesanansumberdaya = hasil

    response = {"data_pesanansumberdaya" : data_pesanansumberdaya}
    return render(request, 'detail_pesanansumberdaya.html', response)


def update_pesanansumberdaya(request, my_id):
    if request.method == 'POST':
        form = addKendaraanForm(request.POST)
        if form.is_valid():
            nama_kendaraan = form.cleaned_data['nama_kendaraan']
            nomor_kendaraan = form.cleaned_data['nomor_kendaraan']
            jenis_kendaraan = form.cleaned_data['jenis_kendaraan']
            berat_maksimum = form.cleaned_data['berat_maksimum']

            with connection.cursor() as c:
                c.execute("Set search_path to sidia;")
                c.execute("SELECT kode_status_batch_pengiriman, tanggal "\
                            "FROM riwayat_status_pengiriman rsp join batch_pengiriman bp "\
                            "ON bp.kode=rsp.kode_batch "\
                            "WHERE no_kendaraan='{}' "\
                            "AND rsp.kode_status_batch_pengiriman='PRO' "\
                            "OR rsp.kode_status_batch_pengiriman='OTW';".format(nomor_kendaraan))
                check = namedtuplefetchall(c)
                c.execute("Set search_path to public;")
                if len(check) == 0:
                    try:
                        c.execute("Set search_path to sidia;")
                        c.execute("UPDATE kendaraan set nomor='{}', nama='{}', jenis_kendaraan='{}', berat_maksimum='{}' "
                                  "WHERE nomor='{}';".format(nomor_kendaraan, nama_kendaraan, jenis_kendaraan, berat_maksimum, my_id))
                        c.execute("Set search_path to public;")
                        return HttpResponseRedirect(reverse('kendaraan:read_kendaraan'))
                    except:
                        messages.error(request, 'UPDATE GAGAL')
                else:
                    messages.error(request, 'UPDATE GAGAL')
                    return HttpResponseRedirect(reverse('kendaraan:update_kendaraan'))

    form = addKendaraanForm()
    response = {'form': form, 'nomor':my_id}
    return render(request, 'update_kendaraan.html', response)

def delete_pesanansumberdaya(request, my_id):
    with connection.cursor() as c:
        c.execute("Set search_path to sidia;")
        c.execute("delete from pesanan_sumber_daya where nomor_pesanan='{}';".format(my_id))
        c.execute("Set search_path to public;")

    messages.info(request, 'BERHASIL MENGHAPUS PESANAN SUMBER DAYA DENGAN NOMOR PESANAN {}'.format(my_id))
    response = {}
    return HttpResponseRedirect(reverse('pesanansumberdaya:read_pesanansumberdaya'))