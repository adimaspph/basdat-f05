from django.urls import path
from .views import *

app_name = 'lokasi'

urlpatterns = [
	path('lokasi/create', create_lokasi, name='create_lokasi'),
	path('lokasi/read', read_lokasi, name='read_lokasi'),
	path('lokasi/update/<str:my_id>', update_lokasi, name='update_lokasi'),
]