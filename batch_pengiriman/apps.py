from django.apps import AppConfig


class BatchPengirimanConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'batch_pengiriman'
