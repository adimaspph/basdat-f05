from django.urls import path
from .views import *

app_name = 'pesanansumberdaya'

urlpatterns = [
	path('pesanansumberdaya/create', create_pesanansumberdaya, name='create_pesanansumberdaya'),
	path('pesanansumberdaya/read', read_pesanansumberdaya, name='read_pesanansumberdaya'),
	path('pesanansumberdaya/update/<str:my_id>', update_pesanansumberdaya, name='update'),
    path('pesanansumberdaya/detail/<str:my_id>', detail_pesanansumberdaya, name='detail'),
	path('pesanansumberdaya/delete/<str:my_id>', delete_pesanansumberdaya, name='delete'),
	path('supplier/get-item-by-supplier', get_item_by_supplier, name='get-item-by-supplier'),
]