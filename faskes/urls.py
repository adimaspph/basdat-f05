from django.urls import path
from .views import *

app_name = 'faskes'

urlpatterns = [
	path('faskes/read', read_faskes, name='read_faskes'),
	path('faskes/delete/<str:my_id>', delete_faskes, name='delete'),
]