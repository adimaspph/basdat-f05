from django.urls import path
from .views import *

app_name = 'kendaraan'

urlpatterns = [
	path('kendaraan/add', add_kendaraan, name='add_kendaraan'),
	path('kendaraan/read', read_kendaraan, name='read_kendaraan'),
	path('kendaraan/update/<str:my_id>', update_kendaraan, name='update'),
	path('kendaraan/delete/<str:my_id>', delete_kendaraan, name='delete'),
]