from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def read_faskes(request):
    with connection.cursor() as c:
        c.execute("Set search_path to sidia;")
        c.execute("select * from faskes left outer join lokasi on id_lokasi=id;")
        hasil = namedtuplefetchall(c)
        print(hasil)
        c.execute("Set search_path to public;")
        data_faskes = hasil

    response = {"data_faskes" : data_faskes}
    return render(request, 'read_faskes.html', response)

def delete_faskes(request, my_id):

    if check_status_available(my_id):
        with connection.cursor() as c:
            c.execute("Set search_path to sidia;")
            c.execute("delete from faskes where kode_faskes_nasional='{}';".format(my_id))
            c.execute("Set search_path to public;")
            messages.info(request, 'BERHASIL MENGHAPUS STOK {}'.format(my_id))
    else:
        messages.error(request, 'GAGAL MENGHAPUS STOK {}'.format(my_id))

    response = {}
    return HttpResponseRedirect(reverse('faskes:read_faskes'))

def check_status_available(nomor_kendaraan):
    with connection.cursor() as c:
        c.execute("Set search_path to sidia;")
        c.execute("SELECT kode_status_batch_pengiriman " \
                  "FROM riwayat_status_pengiriman rsp join batch_pengiriman bp " \
                  "ON bp.kode=rsp.kode_batch " \
                  "WHERE no_kendaraan='{}' "\
                  "ORDER BY tanggal DESC LIMIt 1;".format(nomor_kendaraan))
        check = namedtuplefetchall(c)
        c.execute("Set search_path to public;")
        print(check)

    if len(check)==0:
        return True
    if check[0].kode_status_batch_pengiriman == "OTW" and check[0].kode_status_batch_pengiriman == "PRO":
        return False
    return True
