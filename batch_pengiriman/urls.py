from django.urls import path
from .views import *

app_name = 'batch_pengiriman'

urlpatterns = [
	path('batch', read_batch, name='read_batch'),
	path('batch/tambah', tambah_batch, name='tambah_batch'),
	path('batch/create/<str:my_id>/<str:nama>', create_batch, name='create_batch'),
	# path('lokasi/update/<str:my_id>', update_lokasi, name='update_lokasi'),
]