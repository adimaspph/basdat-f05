from django.shortcuts import render
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .forms import *

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def add_kendaraan(request):
    if request.method == 'POST':
        form = addKendaraanForm(request.POST)
        if form.is_valid():
            nama_kendaraan = form.cleaned_data['nama_kendaraan']
            nomor_kendaraan = form.cleaned_data['nomor_kendaraan']
            jenis_kendaraan = form.cleaned_data['jenis_kendaraan']
            berat_maksimum = form.cleaned_data['berat_maksimum']

            # print("INSERT INTO KENDARAAN VALUES ({}, "{}", "{}", {});".format(nomor_kendaraan, nama_kendaraan, jenis_kendaraan, berat_maksimum))

            try:
                with connection.cursor() as c:
                    c.execute("Set search_path to sidia;")
                    c.execute("INSERT INTO KENDARAAN VALUES ('{}', '{}', '{}', '{}');".format(nomor_kendaraan, nama_kendaraan, jenis_kendaraan, berat_maksimum))
                    c.execute("Set search_path to public;")
                    messages.info(request, 'PENAMBAHAN BERHASIL')
            except:
                messages.error(request, 'PENAMBAHAN GAGAL')
                return HttpResponseRedirect(reverse('kendaraan:add_kendaraan'))

            return HttpResponseRedirect(reverse('kendaraan:read_kendaraan'))

    form = addKendaraanForm()
    response = {'form': form}
    return render(request, 'add_kendaraan.html', response)

def read_kendaraan(request):
    with connection.cursor() as c:
        c.execute("Set search_path to sidia;")
        c.execute("select * from kendaraan;")
        hasil = namedtuplefetchall(c)
        print(hasil)
        c.execute("Set search_path to public;")
        data_kendaraan = hasil

    response = {"data_kendaraan" : data_kendaraan}
    return render(request, 'read_kendaraan.html', response)

def update_kendaraan(request, my_id):
    if request.method == 'POST':
        form = addKendaraanForm(request.POST)
        if form.is_valid():
            nama_kendaraan = form.cleaned_data['nama_kendaraan']
            nomor_kendaraan = form.cleaned_data['nomor_kendaraan']
            jenis_kendaraan = form.cleaned_data['jenis_kendaraan']
            berat_maksimum = form.cleaned_data['berat_maksimum']

            with connection.cursor() as c:
                # c.execute("Set search_path to sidia;")
                # c.execute("SELECT kode_status_batch_pengiriman, tanggal "\
                #             "FROM riwayat_status_pengiriman rsp join batch_pengiriman bp "\
                #             "ON bp.kode=rsp.kode_batch "\
                #             "WHERE no_kendaraan='{}' "\
                #             "AND rsp.kode_status_batch_pengiriman='PRO' "\
                #             "OR rsp.kode_status_batch_pengiriman='OTW';".format(nomor_kendaraan))
                # check = namedtuplefetchall(c)
                # c.execute("Set search_path to public;")
                if check_status_available(my_id):
                    try:
                        c.execute("Set search_path to sidia;")
                        c.execute("UPDATE kendaraan set nomor='{}', nama='{}', jenis_kendaraan='{}', berat_maksimum='{}' "
                                  "WHERE nomor='{}';".format(nomor_kendaraan, nama_kendaraan, jenis_kendaraan, berat_maksimum, my_id))
                        c.execute("Set search_path to public;")
                        messages.info(request, 'BERHASIL MENGUPDATE KENDARAAN {}'.format(my_id))
                        return HttpResponseRedirect(reverse('kendaraan:read_kendaraan'))
                    except:
                        messages.error(request, 'UPDATE GAGAL')
                else:
                    messages.error(request, 'KENDARAAN TIDAK AVAILABLE')
                    return HttpResponseRedirect(reverse('kendaraan:update_kendaraan'))

    form = addKendaraanForm()
    response = {'form': form, 'nomor':my_id}
    return render(request, 'update_kendaraan.html', response)

def delete_kendaraan(request, my_id):

    if check_status_available(my_id):
        with connection.cursor() as c:
            c.execute("Set search_path to sidia;")
            c.execute("delete from kendaraan where nomor='{}';".format(my_id))
            c.execute("Set search_path to public;")
            messages.info(request, 'BERHASIL MENGHAPUS KENDARAAN {}'.format(my_id))
    else:
        messages.error(request, 'GAGAL MENGHAPUS KENDARAAN {}'.format(my_id))

    response = {}
    return HttpResponseRedirect(reverse('kendaraan:read_kendaraan'))

def check_status_available(nomor_kendaraan):
    with connection.cursor() as c:
        c.execute("Set search_path to sidia;")
        c.execute("SELECT kode_status_batch_pengiriman " \
                  "FROM riwayat_status_pengiriman rsp join batch_pengiriman bp " \
                  "ON bp.kode=rsp.kode_batch " \
                  "WHERE no_kendaraan='{}' "\
                  "ORDER BY tanggal DESC LIMIt 1;".format(nomor_kendaraan))
        check = namedtuplefetchall(c)
        c.execute("Set search_path to public;")
        print(check)

    if len(check)==0:
        return True
    if check[0].kode_status_batch_pengiriman == "OTW" and check[0].kode_status_batch_pengiriman == "PRO":
        return False
    return True
