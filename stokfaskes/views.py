from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .forms import *

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def create_stokfaskes(request):
    if request.method == 'POST':
        form = createStok(request.POST)
        if form.is_valid():
            kode_faskes = form.cleaned_data['kode_faskes']
            kode_item = form.cleaned_data['kode_item']
            jumlah = form.cleaned_data['jumlah']

            try:
                with connection.cursor() as c:
                    c.execute("Set search_path to sidia;")
                    c.execute("UPDATE STOK_FASKES SET JUMLAH ('{}', '{}', '{}');".format(kode_faskes, kode_item, jumlah))
                    c.execute("Set search_path to public;")
                    messages.info(request, 'PENAMBAHAN BERHASIL')
            except:
                messages.error(request, 'PENAMBAHAN GAGAL')
                return HttpResponseRedirect(reverse('stokfaskes:create_stokfaskes'))

            return HttpResponseRedirect(reverse('stokfaskes:read_stokfaskes'))

    form = createStok()
    response = {'form': form}
    return render(request, 'create_stokfaskes.html', response)

def read_stokfaskes(request):
    with connection.cursor() as c:
        c.execute("Set search_path to sidia;")
        c.execute("select * from stok_faskes;")
        hasil = namedtuplefetchall(c)
        print(hasil)
        c.execute("Set search_path to public;")
        data_stokfaskes = hasil

    response = {"data_stokfaskes" : data_stokfaskes}
    return render(request, 'read_stokfaskes.html', response)

def detail_pesanansumberdaya(request, my_id):
    with connection.cursor() as c:
        c.execute("Set search_path to sidia;")
        c.execute("select * from pesanan_sumber_daya left outer join transaksi_sumber_daya on nomor_pesanan = nomor;")
        hasil = namedtuplefetchall(c)
        print(hasil)
        detail_item = -1
        for item in hasil:
            if str(item.nomor) == my_id:
                detail_item = item
                break
        print(my_id)
        print(detail_item)

        c.execute("select * from daftar_item where no_transaksi_sumber_daya = %s;", my_id)
        hasil = namedtuplefetchall(c)
        print(hasil)
        daftar_pesanan = hasil
    
        kode_item = hasil[0].kode_item_sumber_daya
        c.execute("select * from item_sumber_daya where kode = '{}';".format(kode_item))
        hasil = namedtuplefetchall(c)
        print(hasil)
        itemsup = hasil[0]
        
        c.execute("Set search_path to public;")

    response = {
                "id" : my_id,
                "petugas" : detail_item.username_admin_satgas,
                "tanggal" : detail_item.tanggal,
                "username_supplier" : itemsup.username_supplier,
                "total_harga" : detail_item.total_harga,
                "daftar_pesanan_item" : daftar_pesanan}
    return render(request, 'detail_pesanansumberdaya.html', response)

def update_stokfaskes(request, my_id):
    with connection.cursor() as c:
                c.execute("Set search_path to sidia;")
                c.execute("select * from stok_faskes where kode_faskes = '{}';".format(my_id))
                hasil = namedtuplefetchall(c)
                print(hasil)
                daftar_stok = -1
                for item in hasil:
                    if str(item.kode_faskes) == my_id:
                        daftar_stok = item
                        break

    if request.method == 'POST':
        form = updateStok(request.POST)
        if form.is_valid():
            jumlahbaru = form.cleaned_data['jumlah']
            with connection.cursor() as c:
                if check_status_available(my_id):
                    try:
                        c.execute("UPDATE stok_faskes set jumlah = {} WHERE kode_faskes='{}';".format(jumlahbaru, my_id))
                        c.execute("Set search_path to public;")
                        messages.info(request, 'BERHASIL MENGUPDATE KENDARAAN {}'.format(my_id))
                        return HttpResponseRedirect(reverse('stokfaskes:read_stokfaskes'))
                    except:
                        messages.error(request, 'UPDATE GAGAL')
                else:
                    messages.error(request, 'KENDARAAN TIDAK AVAILABLE')
                    return HttpResponseRedirect(reverse('stokfaskes:update_stokfaskes'))

    form = updateStok()
    response = {'form': form, 
                'kode_faskes':my_id,
                'id':daftar_stok.kode_faskes,
                'item':daftar_stok.kode_item_sumber_daya}
    return render(request, 'update_stokfaskes.html', response)

def delete_stokfaskes(request, my_id):

    if check_status_available(my_id):
        with connection.cursor() as c:
            c.execute("Set search_path to sidia;")
            c.execute("delete from stok_faskes where kode_faskes='{}';".format(my_id))
            c.execute("Set search_path to public;")
            messages.info(request, 'BERHASIL MENGHAPUS STOK {}'.format(my_id))
    else:
        messages.error(request, 'GAGAL MENGHAPUS STOK {}'.format(my_id))

    response = {}
    return HttpResponseRedirect(reverse('stokfaskes:read_stokfaskes'))

def check_status_available(nomor_kendaraan):
    with connection.cursor() as c:
        c.execute("Set search_path to sidia;")
        c.execute("SELECT kode_status_batch_pengiriman " \
                  "FROM riwayat_status_pengiriman rsp join batch_pengiriman bp " \
                  "ON bp.kode=rsp.kode_batch " \
                  "WHERE no_kendaraan='{}' "\
                  "ORDER BY tanggal DESC LIMIt 1;".format(nomor_kendaraan))
        check = namedtuplefetchall(c)
        c.execute("Set search_path to public;")
        print(check)

    if len(check)==0:
        return True
    if check[0].kode_status_batch_pengiriman == "OTW" and check[0].kode_status_batch_pengiriman == "PRO":
        return False
    return True
