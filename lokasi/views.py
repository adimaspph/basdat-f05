from django.shortcuts import render
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .forms import *

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def create_lokasi(request):
    if request.method == 'POST':
        form = createLokasiForm(request.POST)
        # if form.is_valid():
        id = generate_id()
        # print(form.error())
        provinsi = form['provinsi'].value()
        kabupaten = form['kabupaten'].value()
        kecamatan = form['kecamatan'].value()
        kelurahan = form['kelurahan'].value()
        jalan = form['jalan'].value()

        # print("INSERT INTO KENDARAAN VALUES ({}, "{}", "{}", {});".format(nomor_kendaraan, nama_kendaraan, jenis_kendaraan, berat_maksimum))

        try:
            # print("INSERT INTO LOKASI VALUES ('{}', '{}', '{}', '{}', '{}', '{}');".format(id(), provinsi, kabupaten, kecamatan, kelurahan, jalan))
            with connection.cursor() as c:
                c.execute("set search_path to sidia;")
                c.execute("INSERT INTO LOKASI VALUES ('{}', '{}', '{}', '{}', '{}', '{}');".format(id, provinsi, kabupaten, kecamatan, kelurahan, jalan))
                c.execute("Set search_path to public;")
                messages.info(request, 'PENAMBAHAN BERHASIL')
        except:
            messages.error(request, 'PENAMBAHAN GAGAL')
            return HttpResponseRedirect(reverse('lokasi:create_lokasi'))

        return HttpResponseRedirect(reverse('lokasi:read_lokasi'))

        # else:
        #     print("not valid")

    form = createLokasiForm()
    response = {'form': form}
    return render(request, "create_lokasi.html", response)

def read_lokasi(request):
    with connection.cursor() as c:
        c.execute("Set search_path to sidia;")
        c.execute("select * from lokasi order by id ASC;")
        hasil = namedtuplefetchall(c)
        # print(hasil)
        c.execute("Set search_path to public;")
        data_lokasi = hasil

    response = {"data_lokasi": data_lokasi}
    return render(request, "read_lokasi.html", response)

def update_lokasi(request, my_id):
    if request.method == 'POST':
        form = updateLokasiForm(request.POST)
        if form.is_valid():
            id = my_id
            provinsi = form['provinsi'].value()
            kabupaten = form['kabupaten'].value()
            kecamatan = form['kecamatan'].value()
            kelurahan = form['kelurahan'].value()
            jalan = form['jalan'].value()

            try:
                # print("INSERT INTO LOKASI VALUES ('{}', '{}', '{}', '{}', '{}', '{}');".format(id(), provinsi, kabupaten, kecamatan, kelurahan, jalan))
                with connection.cursor() as c:
                    c.execute("set search_path to sidia;")
                    c.execute("UPDATE LOKASI "\
                              "SET provinsi='{}',"\
                              "kabkot='{}',"\
                              "kecamatan='{}', kelurahan='{}',"\
                              "jalan_no='{}' "\
                              "WHERE id='{}';".format(provinsi, kabupaten, kecamatan, kelurahan, jalan, id))
                    c.execute("Set search_path to public;")
                    messages.info(request, 'UPDATE BERHASIL')
                    return HttpResponseRedirect(reverse('lokasi:read_lokasi'))

            except:
                messages.error(request, 'PENAMBAHAN GAGAL')
                return HttpResponseRedirect(reverse('lokasi:create_lokasi'))

    form = updateLokasiForm()
    response = {'form': form, 'lokasi_id':my_id}
    return render(request, "update_lokasi.html", response)


