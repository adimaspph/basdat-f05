from django import forms
from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def generate_lokasi():
    result = []
    with connection.cursor() as c:
        c.execute("Set search_path to sidia;")
        c.execute("SELECT id FROM lokasi ORDER BY id ASC;")
        data_id = namedtuplefetchall(c)
        c.execute("Set search_path to public;")

        for id in data_id:
            result.append(id.id)

    return result

def generate_kendaraan():
    result = []
    with connection.cursor() as c:
        c.execute("Set search_path to sidia;")
        c.execute("SELECT nomor FROM kendaraan;")
        data_id = namedtuplefetchall(c)
        c.execute("Set search_path to public;")
        for id in data_id:
            result.append(id.nomor)
    return result

def generate_petugas_distribusi():
    result = []
    with connection.cursor() as c:
        c.execute("Set search_path to sidia;")
        c.execute("SELECT username FROM petugas_distribusi;")
        data_id = namedtuplefetchall(c)
        c.execute("Set search_path to public;")
        for id in data_id:
            result.append(id.username)
    return result

def get_lokasi():
    result = [tuple([i, i]) for i in generate_lokasi()]
    return result

def get_kendaraan():
    result = [tuple([i, i]) for i in generate_kendaraan()]
    return result

def get_petugas():
    result = [tuple([i, i]) for i in generate_petugas_distribusi()]
    return result



class createBatchForm(forms.Form):
    kendaraan = forms.CharField(label="Kendaraan", widget=forms.Select(choices=get_kendaraan()))
    petugas = forms.CharField(label="Petugas Distribusi", widget=forms.Select(choices=get_petugas()))

    asal = forms.CharField(label="Lokasi Asal Pengiriman", widget=forms.Select(choices=get_lokasi()))

    tujuan = forms.CharField(label="Lokasi Tujuan Pengiriman", widget=forms.Select(choices=get_lokasi()))






