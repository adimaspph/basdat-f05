from django import forms
from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def generate_id():
    with connection.cursor() as c:
        c.execute("Set search_path to sidia;")
        c.execute("SELECT id FROM lokasi ORDER BY id DESC LIMIT 1;")
        last_id = namedtuplefetchall(c)
        c.execute("Set search_path to public;")
        if len(last_id)==0:
            return "Loc01"
        number = last_id[0].id.split("Loc")

    return "Loc{0:02d}".format((int(number[1]) + 1))

prov = ['Aceh', 'Sumatera Utara', 'Sumatera Barat', 'Riau', 'Jambi', 'Sumatera Selatan', 'Bengkulu', 'Lampung', 'Kepulauan Bangka Belitung', 'Kepulauan Riau', 'Dki Jakarta', 'Jawa Barat', 'Jawa Tengah', 'Di Yogyakarta', 'Jawa Timur', 'Banten', 'Bali', 'Nusa Tenggara Barat', 'Nusa Tenggara Timur', 'Kalimantan Barat', 'Kalimantan Tengah', 'Kalimantan Selatan', 'Kalimantan Timur', 'Kalimantan Utara', 'Sulawesi Utara', 'Sulawesi Tengah', 'Sulawesi Selatan', 'Sulawesi Tenggara', 'Gorontalo', 'Sulawesi Barat', 'Maluku', 'Maluku Utara', 'Papua Barat', 'Papua']

def get_prov():
    result = [tuple([i, i]) for i in prov]
    return result

class createLokasiForm(forms.Form):

    id = forms.CharField(disabled = True, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'value': generate_id(),
        'type' : 'text',
    }))

    provinsi = forms.CharField(label="Provinsi", widget=forms.Select(choices=get_prov()))

    kabupaten = forms.CharField(label = "Kabupaten/Kota",widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kabupaten/Kota',
        'type': 'text',
        'required': True,
    }))

    kecamatan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kecamatan',
        'type': 'text',
        'required': True,
    }))

    kelurahan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kelurahan',
        'type': 'text',
        'required': True,
    }))

    jalan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Jalan',
        'type': 'text',
        'required': True,
    }))

class updateLokasiForm(forms.Form):

    provinsi = forms.CharField(label="Provinsi", widget=forms.Select(choices=get_prov()))

    kabupaten = forms.CharField(label = "Kabupaten/Kota",widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kabupaten/Kota',
        'type': 'text',
        'required': True,
    }))

    kecamatan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kecamatan',
        'type': 'text',
        'required': True,
    }))

    kelurahan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kelurahan',
        'type': 'text',
        'required': True,
    }))

    jalan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Jalan',
        'type': 'text',
        'required': True,
    }))






