from django.urls import path
from .views import *

app_name = 'authentication'
urlpatterns = [
	path('', index, name='index'),
	path('login', login, name='login'),
	path('logout', logout, name='logout'),
	path('register',register,name='register'),
    path('register_admin', register_admin, name='register_admin'),
	path('register_supplier', register_supplier, name='register_supplier'),
	path('register_petugas_distribusi', register_petugas_distribusi, name='register_petugas_distribusi'),
	path('register_petugas_faskes', register_petugas_faskes, name='register_petugas_faskes'),
	path('dashboard', dashboard, name='dashboard'),

]
