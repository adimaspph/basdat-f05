from django import forms

kode = ['113639894287', '143089766565', '274987649790', '821946242720', '612034310135', '113639894287', '143089766565', '274987649790', '821946242720', '612034310135']
item = ['ITM1', 'ITM2', 'ITM3', 'ITM4', 'ITM5', 'ITM6', 'ITM7', 'ITM8', 'ITM9', 'ITM10']

def get_kode():
    result = [tuple([i, i]) for i in kode]
    return result

def get_item():
    result = [tuple([i, i]) for i in item]
    return result

class createStok(forms.Form):

    kode_faskes = forms.CharField(label="kode_faskes", widget=forms.Select(choices=get_kode()))

    kode_item = forms.CharField(label="kode_item", widget=forms.Select(choices=get_item()))

    jumlah = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Jumlah',
        'type': 'number',
        'required': True,
    }))

